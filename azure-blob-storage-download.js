const AzureBlobStorage = require('./azure-blob-storage-class');

module.exports = function(RED) {
    function azureBlobStorageDownload(config) {
        RED.nodes.createNode(this, config);

        var node = this;
        this.config = RED.nodes.getNode(config.storageConfig);
        if (this.config) {
            RED.log.info(`Storage Config Name: ${this.config.name}`);
        } else {
            this.error('Missing config setting');
        }
        const azureBlobStorageIns = new AzureBlobStorage(this, this.config.credentials.accountName, this.config.credentials.accountKey);

        this.on('input', async (msg) => {
            try {
                const options = {
                    filePathToStore: config.filePathToStore,
                    // Allow blobName to be overwritten by msg.blobName
                    blobName: msg.blobName || config.blobName,
                    containerName: config.containerName,
                };
                const res = await azureBlobStorageIns.runDownload(config.mode, options);
                msg.payload = res;
                this.send(msg);
            } catch (e) {
                // Clear status in the node
                this.status({});
                // Send error to catch node, original msg object must be provided
                this.error(e.message, msg);
            }
        });
    }

    RED.nodes.registerType("AzureStorageDownload", azureBlobStorageDownload, {
        credentials: {
            accountName: { type: 'text' },
            accountKey: { type: 'password' }
        },
    });
}