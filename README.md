# node-red-azure-storage
## Introduction
**node-red-azure-storage** is a collection of nodes that perform uploading and downloading blob file from [Microsoft Azure Blob Storage](https://azure.microsoft.com/en-us/services/storage/blobs/).
### Installation
`npm install @blokcert/node-red-azure-storage`

### History
#### 1.1.0
- Create a Config Node to store connection configuration, including Account Name and Key.
- Fix the output message to retain the input message (_msgid and msg).
- Include the **fileurl** in the Upload node's output message.

### Project Information
This project is a modified version of the [@blokcert/node-red-azure-storage](https://flows.nodered.org/node/@blokcert/node-red-azure-storage). The original project was an application for Azure Blob Storage Upload/Doownload, and I've made some adjustments to it to fit specific requirements.

The original project is licensed under the MIT License, and my modifications to the code also follow that license.

### Original Author
- Original Project Author: [intres-dev](https://www.npmjs.com/~intres-dev), [Tsung Jui Wang](https://www.npmjs.com/~tsung-jui-wang)
- Original Project Link: [@intres/node-red-azure-storage](https://www.npmjs.com/package/@intres/node-red-azure-storage)


### About us
The [Blokcert Technology Inc.](https://www.cblok.biz/?lang=en)
Blokcert Technology Inc. was founded in 2018. Originally focused on software services and energy consulting, the company expanded into the electric vehicle charging market development in 2021. Operating under the brand "Area Fast Charge," we aim to accelerate the growth of the electric vehicle trend by providing top-notch charging services.

The [Internet of Things Research (INTRES) Group](https://github.com/UWTINTRES)
at the University of Washington Tacoma (UWT) developed and maintains this package to promote Internet of Things (IoT) research and teaching. This package seeks to accelerate the adoption of IoT concepts by developing a simple mechanism to increase the productivity of researchers, software engineers, developers, and data scientists.


### Example usage of Upload node
The example flow is shown as follows:

![Upload example flow](https://gitlab.com/blokcert/node-red-azure-storage/-/raw/main/img/upload/upload-example-flow.png)

1. Drag the `Upload` node from section `azure-blob-storage`.
2. In the editor section, select which mode to use and complete required inputs. The example below
shows what the inputs should look like. In this example, under `file` mode, the `Upload` node will try to locate the file `/Users/tsungjui/.node-red/music.wav`, upload it to Azure Blob Storage, under container `test`
   and name the blob `hello.wav`.
   
![Upload node configuration](https://gitlab.com/blokcert/node-red-azure-storage/-/raw/main/img/upload/upload-file.png)

3. Click Deploy in the top right corner.
4. Fire the inject node, and a response should be presented in the node-red debug tab.
   ![Upload response](https://gitlab.com/blokcert/node-red-azure-storage/-/raw/main/img/upload/upload-response.png)

### Example usage of Download node
The example flow is shown as follows:

![Download example flow](https://gitlab.com/blokcert/node-red-azure-storage/-/raw/main/img/download/download-example-flow.png)

In this example, we will download the blob `hello.wav` uploaded in the above example as binary content, and upload that binary content
to the container `test`, and name the blob `hello1.wav` using `Upload` node binary mode.

1. Drag `Upload` and `Download` node from section `azure-blob-storage`.
2. In the `Download` node's editor section, complete all the required inputs. In this example, we will try to download the blob named `hello.wav` in container `test`
   as binary content(by selecting `binary` mode).
   
   ![Download node configuration](https://gitlab.com/blokcert/node-red-azure-storage/-/raw/main/img/download/download-file.png)
   
3. In the `Upload` node's editor section, complete all the required inputs. In this example, the node will take `msg.payload` from `Download` node and upload that binary content to the same container `test`, but name it
   to `hello1.wav`.
   
   ![Upload binary configuration](https://gitlab.com/blokcert/node-red-azure-storage/-/raw/main/img/download/upload-binary.png)

#### Disclaimer
INTRES and UWT are not responsible for the usage or utilization of these packages. They are meant to promote IoT research and education. IoT service providers may require additional verification steps to utilize the features outlined in these packages. We are not in any way responsible for the misuse of these packages. For more details on the service agreement and terms, please click [here](https://azure.microsoft.com/en-us/support/legal/).
