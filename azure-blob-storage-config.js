module.exports = function(RED) {
    // config node   
    function azureBlobStorageConfigNode(n) {
        RED.nodes.createNode(this, n);
        this.name = n.name;
        this.credentials.accountName = n.accountName;
        this.credentials.accountKey = n.accountKey;
    }

    RED.nodes.registerType("AzureStorage", azureBlobStorageConfigNode, {
        credentials: {
            accountName: { type: 'text' },
            accountKey: { type: 'password' }
        },
    });
}
